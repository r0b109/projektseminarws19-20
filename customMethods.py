# offers communication methods for the opc-ua server
# contains the logic for the comm. process like rate and necessary variables
# uses the adapter for the mapping details
# https://dresden-elektronik.github.io/deconz-rest-doc/lights/

from opcua import uamethod
from adapter import restAdapter


@uamethod
def shift():
    endpoint = "test"  # server.getVar
    rest = restAdapter(endpoint, "put", "timeout=10", False, "", "")

    status = True  # sever.getVar
    if status is True:
        rest.add_param("on", "false")
    else:
        rest.add_param("on", "true")

    rest.call_endpoint()
    # status.setVar(rest.get_returnValue("on"))


@uamethod
def changeColor():
    pass


# setColor -> werte vordefiniert ? Bspw. var color: red -> json.put mit

"""
# read py scripts from folder and create funcs in sloop ?
def add(__instance, idx, dummy):
    pass


def getNode():
    pass


class method:
    idx = None
    name = ""
    endpoint = ""
    method = ""
    type = ""
"""
